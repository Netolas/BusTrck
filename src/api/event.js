import request from '@/utils/request'

export function getEvents (params) {
  return request({
    url: '/events',
    method: 'get',
    params
  })
}
