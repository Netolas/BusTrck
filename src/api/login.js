import request from '@/utils/request'

export function login (username, password) {
  return request({
    url: '/auth',
    method: 'post',
    auth: {
      username: username,
      password: password
    },
    data: {
      access_token: 'BUnYQTFGGEjfWOqA73GmQ1W09SVp1QqI'
    }
  })
}

export function getInfo () {
  return request({
    url: '/users/me',
    method: 'get'
  })
}

export function logout () {
  return request({
    url: '/users/logout',
    method: 'post'
  })
}
