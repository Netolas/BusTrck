/**
 * Created by Netolas on 12/may/18.
 */
import Vue from 'vue'
import Moment from 'moment'

export function createChart (origin) {
  var eventsDay = origin.map(x => {
    x.day = new Moment(x.createdAt).format('YYYY-MM-DD')
    return x
  })
  var eventsByType = Vue._.groupBy(eventsDay, 'tipo')
  var final = [
    {name: 'Entradas', data: Vue._.countBy(eventsByType.i, 'day')},
    {name: 'Salidas', data: Vue._.countBy(eventsByType.o, 'day')}
  ]
  return final
}
export function createMarkers (origin) {
  var markers = origin.map(x => {
    x.position = {lat: 0, lng: 0}
    x.position.lat = parseFloat(x.cord.substring(0, x.cord.indexOf(',')))
    x.position.lng = parseFloat(x.cord.substring(x.cord.indexOf(',') + 1).trim())
    return x
  })
  return markers
}
