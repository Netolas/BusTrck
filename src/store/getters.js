const getters = {
  sidebar: state => state.app.sidebar,
  token: state => state.user.token,
  avatar: state => state.user.avatar,
  name: state => state.user.name,
  role: state => state.user.role,
  user: state => state.user,
  permission_routers: state => state.permission.routers,
  addRouters: state => state.permission.addRouters,
  isLoading: state => state.app.isLoading,
  events: state => state.event.events,
  eventsByType: state => state.event.eventsByType,
  chart1: state => state.event.chart1,
  markers: state => state.event.markers
}
export default getters
