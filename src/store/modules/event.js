import {
  getEvents
} from '@/api/event'
import Vue from 'vue'

import {createChart, createMarkers} from '@/utils'
const event = {
  state: {
    events: [],
    markers: [],
    eventsByType: [],
    chart1: {}
  },
  mutations: {
    SET_EVENTS: (state, events) => {
      state.events = events
    },
    SET_EVENTS_TYPE: (state, events) => {
      state.eventsByType = events
    },
    SET_CHART1: (state, chart) => {
      state.chart1 = chart
    },
    SET_MARKERS: (state, markers) => {
      state.markers = markers
    }
  },
  actions: {
    GetEvents ({ commit, state }) {
      return new Promise((resolve, reject) => {
        getEvents().then(response => {
          const data = Vue._.clone(response)
          commit('SET_EVENTS', data.rows)
          var chart1 = createChart(data.rows)
          var markers = createMarkers(data.rows)
          commit('SET_CHART1', chart1)
          commit('SET_MARKERS', markers)
          commit('SET_EVENTS_TYPE', Vue._.countBy(data.rows, 'tipo'))
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    }
  }
}

export default event
