import router from './router'
import store from './store'
import { getToken } from '@/utils/auth'

const whiteList = ['/login']
router.beforeEach((to, from, next) => {
  store.dispatch('setLoadingState', true)
  if (getToken()) {
    store.dispatch('GetEvents')
    if (to.path === '/login') {
      next({ path: '/' })
    } else {
      if (store.getters.role === '') {
        store.dispatch('GetInfo').then(res => {
          next()
        }).catch(() => {
          store.dispatch('FedLogOut').then(() => {
            next({ path: '/login' })
          })
        })
      } else {
        next()
      }
    }
  } else {
    if (whiteList.indexOf(to.path) !== -1) {
      next()
    } else {
      next('/login')
    }
  }
})

router.afterEach(() => {
  store.dispatch('setLoadingState', false)
})
