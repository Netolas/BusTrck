// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import store from './store'

import fontawesome from '@fortawesome/fontawesome'
import solid from '@fortawesome/fontawesome-free-solid'
import '@/permission'
import Buefy from 'buefy'
import VueChartkick from 'vue-chartkick'
import Chart from 'chart.js'
import VueLodash from 'vue-lodash'
import * as VueGoogleMaps from 'vue2-google-maps'
import GmapCluster from 'vue2-google-maps/dist/components/cluster'
Vue.component('GmapCluster', GmapCluster)
Vue.use(VueGoogleMaps, {
  load: {
    key: 'AIzaSyAgZD1Gu2xGBD4wFUrkFtWltyoYPvU-4Zs',
    libraries: 'places'
  }
})
Vue.use(VueLodash)

Vue.use(VueChartkick, {adapter: Chart})

Vue.use(Buefy)

fontawesome.library.add(solid)

Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  components: {
    App
  },
  template: '<App/>'
})
